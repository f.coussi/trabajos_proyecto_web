
//Variables  
var boton = document.getElementById("btnComentario");
var inputNombre = document.getElementById("IdNombre");
var inputEmail = document.getElementById("IdEmail");
var inputComentario = document.getElementById("IdComentario");
var resultado = document.getElementById("resultado");
var validacionEspacio = document.getElementById("validacionEspacio");
var validacionLongitud = document.getElementById("validacionLongitud");

baseDatos = [];

boton.addEventListener("click", function () {
    var nombre = inputNombre.value;
    var email = inputEmail.value;
    var comentario = inputComentario.value;
    nuevoSujeto = new Persona(nombre, email, comentario);
    validarLongitud();
    validarEspacio(nuevoSujeto);
    vaciarFormulario();
});

function Persona(nombre, email, comentario) {
    this.nombre = nombre;
    this.email = email;
    this.comentario = comentario;
}

function validarEspacio(nuevoSujeto) {
    if (inputNombre.value != "" && inputEmail.value != "" && inputComentario.value != "") {
        baseDatos.push(nuevoSujeto);
        mostrarComentarios(baseDatos);
    } else {
        validacionEspacio.innerHTML = "<div><h3>* Error existen espacios en blanco</h3></div>";
        vaciarFormulario();
    }
}

function validarLongitud(){
    if (inputNombre.value.length > 200 || inputEmail.value.length > 200 || inputComentario.value.length > 200) {
        validacionLongitud.innerHTML = "<div><h3>* Error hay cadenas de texto mayor a 200 caracteres</h3></div>";
    }else{
        validacionLongitud.innerHTML = " ";
    }  
}

function mostrarComentarios(baseDatos) {
    resultado.innerHTML = " ";
    validacionEspacio.innerHTML = " ";
    if (baseDatos.length > 4) {
        baseDatos.shift();
    }
    for (let index = 0; index < baseDatos.length; index++) {
        resultado.innerHTML += "<br><br><div class='alert alert-primary' role='alert'><div class='row'><div><img src='img/anonimo.jpg'></div><div><h1>" + baseDatos[index].nombre + '</h1></div></div>' + baseDatos[index].email + '<br><br>' + baseDatos[index].comentario + "</div>";
    }
}

function vaciarFomulario() {
    inputNombre.value = "";
    inputEmail.value = "";
    inputComentario.value = "";
}
